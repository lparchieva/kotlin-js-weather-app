'use strict';

var webpack = require('webpack');

var config = {
    "mode": "development",
    "context": "/Users/lemaausheva/Downloads/kotlin-js/build/kotlin-js-min/main",
    "entry": {
        "main": "./kotlin-js"
    },
    "output": {
        "path": "/Users/lemaausheva/Downloads/kotlin-js/build/bundle",
        "filename": "[name].bundle.js",
        "chunkFilename": "[id].bundle.js",
        "publicPath": "/"
    },
    "module": {
        "rules": [
            
        ]
    },
    "resolve": {
        "modules": [
            "kotlin-js-min/main",
            "resources/main",
            "/Users/lemaausheva/Downloads/kotlin-js/build/node_modules",
            "node_modules"
        ]
    },
    "plugins": [
        
    ]
};

module.exports = config;

